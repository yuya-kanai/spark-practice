// Run with  :load ./import.scala

val sqlContext = new org.apache.spark.sql.SQLContext(sc)
val df = sqlContext.read.format("jdbc").options( Map("url" -> "jdbc:postgresql://host.docker.internal:5432/longlist2?user=postgres&password=password",  "dbtable" -> "api_company")).load()